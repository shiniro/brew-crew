import 'package:flutter/material.dart';

class BannerWidgetArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var bannerItems = ['burger', 'Cheese Chilly', 'Noodles', 'Pizza'];
    var bannerImages = [
      'assets/img/burger.jpg',
      'assets/img/chessechilly.jpg',
      'assets/img/noodle.jpg',
      'assets/img/pizza.jpg'
    ];

    var screenWidth = MediaQuery.of(context).size.width;

    // viewportFraction : percentage width or height page show in the area, here 80% of the page width
    PageController controller = PageController(viewportFraction: 0.8, initialPage: 1);

    List<Widget> banners = new List<Widget>();

    for (int i = 0; i < bannerItems.length; i++) {
      var bannerView = Padding(
        padding: EdgeInsets.all(10.0),
        child: Container(
          // padding: EdgeInsets.all(10.0), ?
          child: Stack( // position relative, children position absolute
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black38,
                      offset: Offset(2.0, 2.0),
                      blurRadius: 5.0,
                      spreadRadius: 1.0
                    )
                  ]
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                child: Image.asset(
                  bannerImages[i],
                  fit: BoxFit.cover)
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.transparent, Colors.black]
                  )
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      bannerItems[i],
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.white
                      )
                    ),
                    Text(
                      'More than 40% off',
                      style: TextStyle(
                        fontSize: 12.0,
                        color: Colors.white
                      )
                    )
                  ],
                ),
              )
            ],
          ),
        )
      );
      banners.add(bannerView);
    }

    // if possible use pageview itemBuilder
    /* itemBuilder: (context, position) {
      return _buildCalendar();
    },
    */

    return Container(
      width: screenWidth,
      height: screenWidth*9/16,
      child: PageView(
        controller: controller,
        scrollDirection: Axis.horizontal,
        children: banners,
      ),
    );
  }
}